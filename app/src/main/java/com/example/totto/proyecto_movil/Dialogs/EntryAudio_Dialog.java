package com.example.totto.proyecto_movil.Dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.totto.proyecto_movil.DataUpload.AudioUpload;
import com.example.totto.proyecto_movil.Models.Gpic_Entry;
import com.example.totto.proyecto_movil.R;
import com.example.totto.proyecto_movil.StaticMethods;

import java.io.InputStream;

public class EntryAudio_Dialog extends DialogFragment implements AudioUpload.AudioUploadListener {

    private Gpic_Entry gPicEnt;
    private boolean deletable=false;
    private ProgressBar mProgressBar;
    private Button mPlayButton;
    private ImageView mPlayImg;
    private LinearLayout mLinearLayout;
    private MediaPlayer mMp;
    private boolean playingAudio=false;
    private AudioUpload audioUpload;

    EntryInterface.EntryDialogsListener mListener;

    public void setListener(EntryInterface.EntryDialogsListener listener) {
        mListener = listener;
    }

    public EntryAudio_Dialog(){

    }

    public static EntryAudio_Dialog newInstance(Gpic_Entry gPicEnt, boolean deletable){
        EntryAudio_Dialog f = new EntryAudio_Dialog();
        Bundle args = new Bundle();
        args.putSerializable("gPicEnt", gPicEnt);
        args.putBoolean("deletable",deletable);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gPicEnt = (Gpic_Entry) getArguments().getSerializable("gPicEnt");
        deletable = getArguments().getBoolean("deletable");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.entryaudio_diafrag, null);

        mProgressBar = (ProgressBar) v.findViewById(R.id.pbEntryAudio);
        mLinearLayout = (LinearLayout) v.findViewById(R.id.llEntryAudio);
        mLinearLayout.setVisibility(View.GONE);
        mPlayButton = (Button)v.findViewById(R.id.btnPlayEntryAudio);

        mPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMp.seekTo(0);
                mMp.start();
            }
        });

        Button btnDelete = (Button)v.findViewById(R.id.btnDeleteEntryAudio);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                builder.setMessage("Delete Entry?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mListener.onDeleteEntry(gPicEnt);
                                EntryAudio_Dialog.this.dismiss();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).show();
            }
        });

        if(!deletable){
            btnDelete.setVisibility(View.GONE);
        }

        audioUpload = new AudioUpload();
        audioUpload.setmListener(this);
        if (!gPicEnt.getEntryValue().equals("")) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    audioUpload.reproduce(gPicEnt.getEntryValue());
                }
            }).start();

        } else {
            Toast.makeText(getActivity(), "no hay archivo que reproducir", Toast.LENGTH_SHORT).show();
        }

        builder.setView(v)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog dialog = builder.create();
        Window window = dialog.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setGravity(Gravity.CENTER);
        return dialog;
    }


    @Override
    public void onDismiss(DialogInterface dialog) {

        super.onDismiss(dialog);
        mMp.release();

    }

    @Override
    public void onAudioPrepared(MediaPlayer mp) {
        mProgressBar.setVisibility(View.GONE);
        StaticMethods.viewFadeIn(getActivity(),mLinearLayout);
        mMp = mp;
    }
}
