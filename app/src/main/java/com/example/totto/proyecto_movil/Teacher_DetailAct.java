package com.example.totto.proyecto_movil;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.totto.proyecto_movil.Fragments.DetAct_MapFragment;
import com.example.totto.proyecto_movil.Models.Gpic_Activity;
import com.example.totto.proyecto_movil.Models.Gpic_Entry;
import com.example.totto.proyecto_movil.Models.Gpic_Pathing;
import com.example.totto.proyecto_movil.Models.Gpic_Submit;
import com.example.totto.proyecto_movil.RecycleAdapters.th_DetailAdapter;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Teacher_DetailAct extends AppCompatActivity implements
        DetAct_MapFragment.DetActMapFragmentListener, th_DetailAdapter.th_DetailRecListener{

    Firebase myFirebaseRef;

    private List<Gpic_Submit> mData = new ArrayList<>();
    private RecyclerView mrV;
    private th_DetailAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Gpic_Activity mGPicAct;
    private DetAct_MapFragment mapFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teacher__detailact);

        mGPicAct = (Gpic_Activity) getIntent().getSerializableExtra("activity");

        mapFragment = (DetAct_MapFragment)getFragmentManager().
                findFragmentById(R.id.th_detailMap);
        mapFragment.setListener(this);

        myFirebaseRef = new Firebase(MainActivity.fbRoot+"Submits/"+mGPicAct.getName());

        mrV = (RecyclerView) findViewById(R.id.th_detRec);
        mrV.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mrV.setLayoutManager(mLayoutManager);
        mAdapter = new th_DetailAdapter(this,mData);
        mrV.setAdapter(mAdapter);
        mAdapter.setRecyclerClickListner(this);

        setChildListener();

    }

    private void setChildListener(){
        myFirebaseRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Gpic_Submit gPicSub = new Gpic_Submit();

                gPicSub.setStudentID(dataSnapshot.getKey());

                for (DataSnapshot entrySnapshot : dataSnapshot.child("Entries").
                        getChildren() ){
                    Gpic_Entry gPicEnt = entrySnapshot.getValue(Gpic_Entry.class);
                    gPicSub.addEntry(gPicEnt);
                }

                Gpic_Pathing gPicPath = new Gpic_Pathing();
                for(DataSnapshot path : dataSnapshot.child("Pathing").getChildren()){
                    String[] location = path.getValue().toString().split(",");
                    gPicPath.addStep(path.getKey(),location[0],location[1]);
                }
                gPicSub.setSubmitPathing(gPicPath);

                mAdapter.addItem(mData.size(), gPicSub);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                /*
                Gpic_Activity gPicAct = dataSnapshot.getValue(Gpic_Activity.class);
                mapFragment.removeGPicEntMarker(gPicEnt);
                Gpic_Submit gPicSub = new Gpic_Submit();
                gPicSub.setStudentID(dataSnapshot.child("studentID").getValue().toString());
                int index = getIndexOf(gPicSub);
                mAdapter.removeItem(index);
                updateService(-1,gPicAct);
                */
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    private Gpic_Submit mSelectSubmit = null;
    @Override
    public void itemClick(View view, int position) {
        mSelectSubmit = mAdapter.getItem(position);
        mapFragment.addGpicEntMarkers(mSelectSubmit.getSubmitEntries());
        mapFragment.addPathing(mSelectSubmit.getSubmitPathing());
    }

    public void thd_btnBackOnClick(View view) {
        finish();
    }

    @Override
    public void mapReady() {
        mapFragment.initMapFragment(mGPicAct);
        mapFragment.setListener(this);
    }
}
