package com.example.totto.proyecto_movil.ApiImgur.imgurmodel;

import java.io.File;

/**
 * Created by com.example.totto.proyecto_movil on 2/24/15.
 *
 * Basic object for upload.
 */
public class Upload {
  public File image;
  public String title;
  public String description;
  public String albumId;
}
