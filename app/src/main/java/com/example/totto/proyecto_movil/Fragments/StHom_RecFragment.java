package com.example.totto.proyecto_movil.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.totto.proyecto_movil.MainActivity;
import com.example.totto.proyecto_movil.Models.Gpic_Activity;
import com.example.totto.proyecto_movil.R;
import com.example.totto.proyecto_movil.RecycleAdapters.HomepageAdapter;
import com.example.totto.proyecto_movil.StaticMethods;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StHom_RecFragment extends Fragment implements HomepageAdapter.th_HomepageRecListener{

    private List<Gpic_Activity> mData = new ArrayList<>();
    private RecyclerView mrV;
    private HomepageAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private StHomRecFragmentListener mListener;

    public interface StHomRecFragmentListener {
        public void onRecGPicClick(Gpic_Activity gPicAct);
        public void onRecFragmentReady();
    }
    public StHom_RecFragment() {
        // Required empty public constructor
    }
    public static StHom_RecFragment newInstance() {
        StHom_RecFragment fragment = new StHom_RecFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_st_hom__rec, container, false);

        mrV = (RecyclerView) v.findViewById(R.id.st_homRec);
        mrV.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mrV.setLayoutManager(mLayoutManager);
        mAdapter = new HomepageAdapter(getActivity(),mData);
        mrV.setAdapter(mAdapter);
        mAdapter.setRecyclerClickListner(this);
        mListener.onRecFragmentReady();
        return v;
    }

    public void addGpicActItem(Gpic_Activity gPicAct){
        mAdapter.addItem(this.mData.size(), gPicAct);
    }

    public void removeGpicActItem(int index) {
        mAdapter.removeItem(index);
    }

    public void setGPicActItems(List<Gpic_Activity> mData) {
        mAdapter.clearData();
        for(Gpic_Activity gPicAct : mData){
            addGpicActItem(gPicAct);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof StHomRecFragmentListener) {
            mListener = (StHomRecFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void itemClick(View view, int position) {
        mListener.onRecGPicClick(mAdapter.getItem(position));
    }
}
