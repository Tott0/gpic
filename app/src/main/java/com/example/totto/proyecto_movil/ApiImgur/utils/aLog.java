package com.example.totto.proyecto_movil.ApiImgur.utils;

import android.util.Log;

import com.example.totto.proyecto_movil.ApiImgur.Constants;

/**
 * Created by com.example.totto.proyecto_movil on 1/16/15.
 *
 * Basic logger bound to a flag in Constants.java
 */
public class aLog {
  public static void w (String TAG, String msg){
    if(Constants.LOGGING) {
      if (TAG != null && msg != null)
        Log.w(TAG, msg);
    }
  }

}
