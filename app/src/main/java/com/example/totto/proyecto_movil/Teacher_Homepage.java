package com.example.totto.proyecto_movil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.totto.proyecto_movil.Models.Gpic_Activity;
import com.example.totto.proyecto_movil.RecycleAdapters.HomepageAdapter;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.getbase.floatingactionbutton.AddFloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class Teacher_Homepage extends AppCompatActivity implements
        HomepageAdapter.th_HomepageRecListener {

    Firebase myFirebaseRef;

    private final String[] tableSelector = {"Active_Activities/","Closed_Activities/"};

    private List<Gpic_Activity> mData = new ArrayList<>();
    private RecyclerView mrV;
    private HomepageAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ToggleButton btnChange;
    private AddFloatingActionButton mFAB;
    private String mTeacherID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teacher__homepage);

        mTeacherID = LoginSharedPreference.getUserName(Teacher_Homepage.this);

        myFirebaseRef = new Firebase(MainActivity.fbRoot+tableSelector[0]);

        mrV = (RecyclerView) findViewById(R.id.th_homRec);
        mrV.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mrV.setLayoutManager(mLayoutManager);
        mAdapter = new HomepageAdapter(this,mData);
        mrV.setAdapter(mAdapter);

        mAdapter.setRecyclerClickListner(this);

        btnChange = (ToggleButton)findViewById(R.id.th_btnChange);
        mFAB      = (AddFloatingActionButton)findViewById(R.id.th_fab);

        btnChange.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    myFirebaseRef = new Firebase(MainActivity.fbRoot+tableSelector[1]);
                    StaticMethods.viewFadeOut(Teacher_Homepage.this, mFAB);
                }
                else {
                    myFirebaseRef = new Firebase(MainActivity.fbRoot+tableSelector[0]);
                    StaticMethods.viewFadeIn(Teacher_Homepage.this, mFAB);
                }
                setListener();
            }
        });

        setListener();


    }

    private void setListener(){
        myFirebaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    mAdapter.clearData();
                    for (DataSnapshot postSnapshot : snapshot.getChildren())
                    {
                        Gpic_Activity gPicAct = postSnapshot.getValue(Gpic_Activity.class);
                        mAdapter.addItem(mData.size(), gPicAct);
                    }
                }
                catch (Exception e){
                    Toast.makeText(Teacher_Homepage.this,"Error inesperado",Toast.LENGTH_SHORT).show();
                    Log.e("asd",e.getLocalizedMessage().toString());
                }

            }

            @Override
            public void onCancelled(FirebaseError error) {
            }
        });
    }

    @Override
    public void itemClick(View view, int position) {
        if (StaticMethods.getConnectivityStatusString(this)
                != StaticMethods.NETWORK_STATUS_NOT_CONNECTED) {
            Intent i = new Intent(this, Teacher_DetailAct.class);
            i.putExtra("activity", mAdapter.getItem(position));
            startActivity(i);
        }
        else {
            Toast.makeText(this,"No Connection",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 1:
                if(resultCode == Activity.RESULT_OK){
                    Gpic_Activity eGpic_Act = (Gpic_Activity) data.
                            getSerializableExtra("data");
                    mAdapter.addItem(mData.size(), eGpic_Act );
                    myFirebaseRef.child(eGpic_Act.getName()).setValue(eGpic_Act);
                }
        }
    }

    public void th_fab_OnClick(View view) {
        if (StaticMethods.getConnectivityStatusString(this)
                != StaticMethods.NETWORK_STATUS_NOT_CONNECTED) {
            Intent i = new Intent(this, Teacher_CreateAct.class);
            i.putExtra("teacherID", "GpicTeacherAdmin");
            startActivityForResult(i, 1);
        }
        else {
            Toast.makeText(this,"No Connection",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.homepage_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.miLogout:
                StaticMethods.attemptLogout(Teacher_Homepage.this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
