package com.example.totto.proyecto_movil.Dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.example.totto.proyecto_movil.R;

public class Student_TextEntry_Dialog extends DialogFragment {

    private EditText mEntryText;

    public interface TextEntryDialogListener{
        public void TextEntryDialogClosed(String text);
    }

    TextEntryDialogListener mListener;

    public void setListener(TextEntryDialogListener listener) {
        mListener = listener;
    }

    public Student_TextEntry_Dialog(){

    }

    public static Student_TextEntry_Dialog newInstance(){
        Student_TextEntry_Dialog f = new Student_TextEntry_Dialog();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.student_textentry_diafrag, null);

        mEntryText = (EditText)v.findViewById(R.id.student_tvEntryText);


        builder.setView(v)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String text = mEntryText.getText().toString();
                        mListener.TextEntryDialogClosed(text);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        AlertDialog dialog = builder.create();
        Window window = dialog.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setGravity(Gravity.BOTTOM);
        return dialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }
}
