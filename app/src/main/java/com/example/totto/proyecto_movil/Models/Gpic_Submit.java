package com.example.totto.proyecto_movil.Models;

/**
 * Created by Totto on 29/04/2016.
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class Gpic_Submit implements Serializable {

    private List<Gpic_Entry> submitEntries;
    private Gpic_Pathing submitPathing;
    private String studentID;

    public Gpic_Submit(){
        submitEntries = new ArrayList<Gpic_Entry>();
        submitPathing = new Gpic_Pathing();
    }

    public Gpic_Submit(List<Gpic_Entry> submitEntries, Gpic_Pathing submitPathing, String studentID) {
        this.submitEntries = submitEntries;
        this.submitPathing = submitPathing;
        this.studentID = studentID;
    }

    public List<Gpic_Entry> getSubmitEntries() {
        return submitEntries;
    }

    public void setSubmitEntries(List<Gpic_Entry> submitEntries) {
        this.submitEntries = submitEntries;
    }

    public Gpic_Pathing getSubmitPathing() {
        return submitPathing;
    }

    public void setSubmitPathing(Gpic_Pathing submitPathing) {
        this.submitPathing = submitPathing;
    }

    public void addEntry (Gpic_Entry entry){
        this.submitEntries.add(entry);
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public void addAllEntries(ArrayList<Gpic_Entry> gPicEnts) {
        this.submitEntries = gPicEnts;
    }
}
