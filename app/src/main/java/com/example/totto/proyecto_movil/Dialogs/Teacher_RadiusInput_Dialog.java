package com.example.totto.proyecto_movil.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.totto.proyecto_movil.R;

public class Teacher_RadiusInput_Dialog extends DialogFragment {

    private int radius;

    public interface RadiusInputDialogListener{
        public void RadiusChange(int radius);
        public void DialogClosed();
    }

    RadiusInputDialogListener mListener;

    public void setListener(RadiusInputDialogListener listener) {
        mListener = listener;
    }

    public Teacher_RadiusInput_Dialog(){

    }

    public static Teacher_RadiusInput_Dialog newInstance(int radius){
        Teacher_RadiusInput_Dialog f = new Teacher_RadiusInput_Dialog();
        Bundle args = new Bundle();
        args.putInt("radius",radius);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        radius = getArguments().getInt("radius");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.teacher_radius_selector_diafrag, null);


        builder.setView(v)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });



        final SeekBar seekBar = (SeekBar)v.findViewById(R.id.th_skRadius);
        final TextView textView = (TextView)v.findViewById(R.id.th_tvRadius);

        seekBar.setProgress(radius/100 - 1);
        textView.setText(Integer.toString(radius));


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                radius = (seekBar.getProgress()+1)*100;
                textView.setText(Integer.toString(radius));
                mListener.RadiusChange(radius);
            }
        });

        AlertDialog dialog = builder.create();
        Window window = dialog.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setGravity(Gravity.BOTTOM);
        return dialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        mListener.DialogClosed();
    }
}
