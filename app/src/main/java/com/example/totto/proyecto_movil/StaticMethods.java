package com.example.totto.proyecto_movil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Totto on 02/05/2016.
 */
public class StaticMethods {

    public static String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static void EditTextDoneAction(TextView v, Activity activity) {
        v.clearFocus();
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void attemptLogout(final Activity activity) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        AlertDialog show = builder.setMessage("Log out?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent i = new Intent(activity, Login.class);
                        activity.startActivity(i);
                        LoginSharedPreference.clear(activity);
                        activity.finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
    }

    public static void viewFadeIn(Activity activity, final View v){
        v.setAlpha(0f);
        v.setVisibility(View.VISIBLE);

        v.animate()
                .alpha(1f)
                .setDuration(activity.getResources().getInteger(android.R.integer.config_shortAnimTime))
                .setListener(null);
    }

    public static void viewFadeOut(Activity activity, final View v){
        v.animate()
                .alpha(0f)
                .setDuration(activity.getResources().getInteger(android.R.integer.config_shortAnimTime))
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        v.setVisibility(View.GONE);
                    }
                });
    }


    //region Network
        private static int TYPE_WIFI = 1;
        private static int TYPE_MOBILE = 2;
        private static int TYPE_NOT_CONNECTED = 0;
        public static final int NETWORK_STATUS_NOT_CONNECTED=0,NETWORK_STAUS_WIFI=1,NETWORK_STATUS_MOBILE=2;

        private static int getConnectivityStatus(final Context context) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

            if (null != activeNetwork && activeNetwork.isConnected()) {
                Log.e("asdreceiver",activeNetwork.getTypeName());
                if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                        return TYPE_WIFI;

                if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                    return TYPE_MOBILE;
            }
            return TYPE_NOT_CONNECTED;
        }

        public static int getConnectivityStatusString(final Context context) {
            int conn = getConnectivityStatus(context);
            int status = 0;
            if (conn == TYPE_WIFI) {
                status = NETWORK_STAUS_WIFI;
            } else if (conn == TYPE_MOBILE) {
                status =NETWORK_STATUS_MOBILE;
            } else if (conn == TYPE_NOT_CONNECTED) {
                status = NETWORK_STATUS_NOT_CONNECTED;
            }
            return status;
        }
    //endregion

}
