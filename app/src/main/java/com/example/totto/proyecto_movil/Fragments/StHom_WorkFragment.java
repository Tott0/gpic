package com.example.totto.proyecto_movil.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.totto.proyecto_movil.MainActivity;
import com.example.totto.proyecto_movil.R;
import com.example.totto.proyecto_movil.StaticMethods;
import com.firebase.client.Firebase;

public class StHom_WorkFragment extends Fragment{

    private StHomWorkFragmentListener mListener;
    private boolean checkedState = false;

    public interface StHomWorkFragmentListener {
        public void onToggle(boolean checkedState);
    }


    public StHom_WorkFragment() {
        // Required empty public constructor
    }

    public static StHom_WorkFragment newInstance() {
        StHom_WorkFragment fragment = new StHom_WorkFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_st_hom__work, container, false);

        final Button btnChange = (Button)v.findViewById(R.id.st_btnStartWork);
        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dialogMsg = "";
                if (!checkedState){
                    dialogMsg = "Empezar toma de datos?";
                }
                else {
                    dialogMsg = "terminar toma de datos?";
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(dialogMsg)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (StaticMethods.getConnectivityStatusString(getActivity())
                                        != StaticMethods.NETWORK_STATUS_NOT_CONNECTED || checkedState){
                                    checkedState = !checkedState;
                                    if(!checkedState) {
                                        btnChange.setText("Start");
                                    }else {
                                        btnChange.setText("Stop");
                                    }
                                    mListener.onToggle(checkedState);
                                }else{
                                    Toast.makeText(getActivity(),"No Connection",Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).show();
            }
        });

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof StHomWorkFragmentListener) {
            mListener = (StHomWorkFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
