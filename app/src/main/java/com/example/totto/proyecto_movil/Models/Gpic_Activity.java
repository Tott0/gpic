package com.example.totto.proyecto_movil.Models;

/**
 * Created by Totto on 29/04/2016.
 */

import java.io.Serializable;

@SuppressWarnings("serial")
public class Gpic_Activity implements Serializable {

    private String latitude;
    private String longitude;
    private String description;
    private String name;
    private String radius;
    private String teacherID;

    public Gpic_Activity(){

    }

    public Gpic_Activity(String latitude, String longitude, String description, String name, String radius, String teacherID) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
        this.name = name;
        this.radius = radius;
        this.teacherID = teacherID;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }
}
