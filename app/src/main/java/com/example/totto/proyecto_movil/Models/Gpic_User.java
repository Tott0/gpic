package com.example.totto.proyecto_movil.Models;

/**
 * Created by Totto on 29/04/2016.
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class Gpic_User implements Serializable {

    private String userID;
    private String userName;
    private String userPass;
    private String userType;



    public Gpic_User(){

    }

    public Gpic_User(String userID, String userName, String userPass, String userType) {
        this.userID   = userID;
        this.userName = userName;
        this.userPass = userPass;
        this.userType = userType;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
