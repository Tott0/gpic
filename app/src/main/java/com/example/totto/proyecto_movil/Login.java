package com.example.totto.proyecto_movil;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.totto.proyecto_movil.Models.Gpic_User;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class Login extends AppCompatActivity {

    private boolean name;
    private boolean pass;

    private EditText tvUserName;
    private EditText tvPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tvUserName = (EditText) findViewById(R.id.tvLoginUsername);

        tvPass = (EditText) findViewById(R.id.tvLoginPass);
        tvPass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                StaticMethods.EditTextDoneAction(v,Login.this);
                }
                return false;
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 0:
                if(resultCode == RESULT_OK){
                    this.finish();
                }
                break;
        }
    }

    public boolean isComplete(){
        pass = !"".equals(tvPass.getText().toString());
        name = !"".equals(tvUserName.getText().toString());
        return pass && name;
    }

    public void btnLoginOnClick(View view) {

        if(!isComplete()){
            if(!name){
                Toast.makeText(this,"Complete el campo username",
                        Toast.LENGTH_SHORT).show();

                tvUserName.setError("Username no puede estar vacío");
            }
            else{
                Toast.makeText(this,"Complete el campo password",
                        Toast.LENGTH_SHORT).show();

                tvPass.setError("Password no puede estar vacío");
            }

        }else{
            loginAttempt();
        }


    }

    private void loginAttempt() {
        final String userName = ((TextView)findViewById(R.id.tvLoginUsername)).getText().toString();
        String userPass = ((TextView)findViewById(R.id.tvLoginPass)).getText().toString();
        final String eUserPass = StaticMethods.MD5(userPass);

        Firebase loginFirebase = new Firebase(MainActivity.fbRoot+"Users/"+userName+"/");
        loginFirebase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    Gpic_User user = dataSnapshot.getValue(Gpic_User.class);
                    String ePass = user.getUserPass();
                    if(ePass.equals(eUserPass)){
                        int type = Integer.parseInt(user.getUserType());
                        Intent i = null;
                        switch (type) {
                            case 0:
                                i = new Intent(Login.this, Student_Homepage.class);
                                i.putExtra("userName", user.getUserID());
                                break;
                            case 1:
                                i = new Intent(Login.this, Teacher_Homepage.class);
                                i.putExtra("userName", user.getUserID());
                        }
                        LoginSharedPreference.setUserName(Login.this,user.getUserID());
                        LoginSharedPreference.setUserType(Login.this,user.getUserType());

                        startActivity(i);
                        Login.this.finish();

                    }
                    else{
                        Toast.makeText(Login.this,"Password incorrecto",Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    Toast.makeText(Login.this,"Username no encontrado",Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }


    public void btnLogRegisterOnClick(View view) {
        Intent i = new Intent(this,Register.class);
        startActivityForResult(i,0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvPass.setText("");
        tvUserName.setText("");
    }

}
