package com.example.totto.proyecto_movil.DataUpload;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Totto on 09/05/2016.
 */
public class AudioRecording {
    private MediaRecorder myAudioRecorder;
    private MediaPlayer mediaPlayer = null;
    private File outputFile = null;
    private int activityId;
    private int userId;

    private static final String AUDIO_RECORDER_FOLDER = "Gpic/Audio";
    private static final String AUDIO_RECORDER_FILE_EXT = ".mp3";

    private AudioRecordingListener mListener;

    public interface AudioRecordingListener{
        public void audioRecorded(File outputFile);
    }

    public void setListener(AudioRecordingListener mListener){
        this.mListener = mListener;
    }

    public AudioRecording() {

    }

    public void starRecording(){
        try {
            if(mediaPlayer != null){
                if(mediaPlayer.isPlaying()){ mediaPlayer.stop();
                }
            }

            outputFile = createAudioFile();
            myAudioRecorder = new MediaRecorder();
            myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            myAudioRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            myAudioRecorder.setOutputFile(outputFile.getAbsolutePath());
            myAudioRecorder.prepare();
            myAudioRecorder.start();

        } catch (IOException e) {e.printStackTrace(); }

    }

    public void stopRecording(){
        if( myAudioRecorder != null){
            myAudioRecorder.stop();
            myAudioRecorder.release();
            myAudioRecorder  = null;

            mListener.audioRecorded(outputFile);
        }
    }

    public void playAudio(String audioName){

        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);
        audioName = file.getAbsolutePath() + "/" + audioName;

        if(mediaPlayer != null){
            if(mediaPlayer.isPlaying()){
                mediaPlayer.stop();
            }
        }

        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(audioName);
            mediaPlayer.prepare();
            mediaPlayer.start();

        }

        catch (IOException e) { e.printStackTrace(); }
    }

    public  void stopAudio(){
        if(mediaPlayer != null){
            if(mediaPlayer.isPlaying()){
                mediaPlayer.stop();
                mediaPlayer = null;
            }
        }
    }

    private File createAudioFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String audioFileName = "AUD_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PODCASTS);
        File audio = File.createTempFile(
                audioFileName,  /* prefix */
                ".mp3",         /* suffix */
                storageDir      /* directory */
        );
        return audio;
    }

}
