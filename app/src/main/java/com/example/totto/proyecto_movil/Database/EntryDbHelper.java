package com.example.totto.proyecto_movil.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.totto.proyecto_movil.Models.Gpic_Activity;
import com.example.totto.proyecto_movil.Models.Gpic_Entry;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Totto on 05/05/2016.
 */
public class EntryDbHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + GpicEntry.FeedEntry.TABLE_NAME + " (" +
                    GpicEntry.FeedEntry._ID + " INTEGER PRIMARY KEY," +
                    GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_LATITUDE + TEXT_TYPE + COMMA_SEP +
                    GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_LONGITUDE + TEXT_TYPE + COMMA_SEP +
                    GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_TYPE + TEXT_TYPE + COMMA_SEP +
                    GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_Value + TEXT_TYPE + COMMA_SEP +
                    GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_Key + TEXT_TYPE + COMMA_SEP +
                    GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_Student + TEXT_TYPE + COMMA_SEP +
                    GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_Activity + TEXT_TYPE +
                    " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + GpicEntry.FeedEntry.TABLE_NAME;
    private SQLiteDatabase mDb;


    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "GPicEntry.db";

    public EntryDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mDb = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void clearDB(){
        mDb.delete(GpicEntry.FeedEntry.TABLE_NAME,null,null);
    }

    public void getTables() {
        Cursor c = mDb.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);

        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                Log.d("asd", "Table Name=> "+c.getString(0));
                c.moveToNext();
            }
        }
    }

    public void addItem (Gpic_Entry gPicEnt, String studentID, String activity){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss", Locale.US);
        String mLastUpdateTime  = sdf.format(new Date());

        ContentValues values = new ContentValues();
        values.put(GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_LATITUDE,gPicEnt.getEntryLatitude());
        values.put(GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_LONGITUDE,gPicEnt.getEntryLongitude());
        values.put(GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_TYPE,gPicEnt.getEntryType());
        values.put(GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_Value,gPicEnt.getEntryValue());
        switch (Integer.parseInt(gPicEnt.getEntryType())){
            case 1:
                mLastUpdateTime = "img_"+mLastUpdateTime;
                break;
            case 2:
                mLastUpdateTime = "aud_"+mLastUpdateTime;
                break;
            case 3:
                mLastUpdateTime = "txt_"+mLastUpdateTime;
                break;
            case 4:
                mLastUpdateTime = "tag_"+mLastUpdateTime;
                break;
        }
        gPicEnt.setEntryKey(mLastUpdateTime);
        values.put(GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_Key,mLastUpdateTime);
        values.put(GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_Student,studentID);
        values.put(GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_Activity,activity);

        long newRowid = insertData(values);
    }

    public long insertData(ContentValues values) {
        return mDb.insert(GpicEntry.FeedEntry.TABLE_NAME, null, values);
    }

    public ArrayList<String[]> readData() {

        Cursor c = mDb.rawQuery("SELECT * FROM " + GpicEntry.FeedEntry.TABLE_NAME,null);
        ArrayList<String[]> data = new ArrayList<>();
        if(c.moveToFirst()){
            do{
                String[] tableRow = new String[7];
                tableRow[0] = c.getString(c.getColumnIndex(GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_Activity));
                tableRow[1] = c.getString(c.getColumnIndex(GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_Student));
                tableRow[2] = c.getString(c.getColumnIndex(GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_Key));
                tableRow[3] = c.getString(c.getColumnIndex(GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_LATITUDE));
                tableRow[4] = c.getString(c.getColumnIndex(GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_LONGITUDE));
                tableRow[5] = c.getString(c.getColumnIndex(GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_TYPE));
                tableRow[6] = c.getString(c.getColumnIndex(GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_Value));
                data.add(tableRow);
            }while (c.moveToNext());
        }
        return data;
    }


    public void removeGpicEntry(String key) {
        mDb.delete(GpicEntry.FeedEntry.TABLE_NAME,
                GpicEntry.FeedEntry._ID+"="+key,
                null);
    }

    public void removeGpicEntry(Gpic_Entry gPicEnt) {
        mDb.delete(GpicEntry.FeedEntry.TABLE_NAME,
                GpicEntry.FeedEntry.COLUMN_NAME_ENTRY_Key+"=?",
                new String[]{gPicEnt.getEntryKey()});
    }
}
