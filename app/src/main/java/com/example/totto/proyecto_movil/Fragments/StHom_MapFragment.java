package com.example.totto.proyecto_movil.Fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.example.totto.proyecto_movil.Dialogs.EntryAudio_Dialog;
import com.example.totto.proyecto_movil.Dialogs.EntryInterface;
import com.example.totto.proyecto_movil.Dialogs.EntryPhoto_Dialog;
import com.example.totto.proyecto_movil.Dialogs.EntryText_Dialog;
import com.example.totto.proyecto_movil.Dialogs.Teacher_RadiusInput_Dialog;
import com.example.totto.proyecto_movil.Models.Gpic_Activity;
import com.example.totto.proyecto_movil.Models.Gpic_Entry;
import com.example.totto.proyecto_movil.Models.Gpic_Pathing;
import com.example.totto.proyecto_movil.R;
import com.example.totto.proyecto_movil.Student_Homepage;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Totto on 30/04/2016.
 */
public class StHom_MapFragment extends MapFragment implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener,
        OnMapReadyCallback, LocationListener {

    private GoogleApiClient mGoogleApiClient;
    private Location mCurrentLocation;
    private LocationRequest mLocationRequest;

    private final int[] MAP_TYPES = {GoogleMap.MAP_TYPE_SATELLITE,
            GoogleMap.MAP_TYPE_NORMAL,
            GoogleMap.MAP_TYPE_HYBRID,
            GoogleMap.MAP_TYPE_TERRAIN,
            GoogleMap.MAP_TYPE_NONE};
    private int curMapTypeIndex = 2;

    private final String MARKER_GPICACT = "GpicAct";
    private final String MARKER_GPICENT = "GpicEnt";
    private final String LOG_TAG        = "asdStHomMap";

    private GoogleMap mGoogleMap;

    private Circle mCircle;
    private int mRadius;
    private boolean workState;

    private Polyline mPolyline;

    private Map<Marker, Gpic_Activity> mGPicActMarkers = new HashMap<>();
    private Map<Marker, Gpic_Entry> mGPicEntMarkers = new HashMap<>();
    private Gpic_Pathing gPicPathing;

    public interface stHomMapFragmentListener {

        public void onMapReady();

        public void onAddStep(String time, String location);
    }
    stHomMapFragmentListener mListener;

    public void setListener(stHomMapFragmentListener listener) {
        mListener = listener;
    }

    public Gpic_Pathing getgPicPathing() {
        return gPicPathing;
    }

    public ArrayList<Gpic_Entry> getGPicEntries() {
        return new ArrayList<Gpic_Entry>(mGPicEntMarkers.values());
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        this.getMapAsync(this);
        gPicPathing = new Gpic_Pathing();
    }

    public void addGPicActMarker(Gpic_Activity gPicAct){
        double lat = Double.parseDouble(gPicAct.getLatitude());
        double lng = Double.parseDouble(gPicAct.getLongitude());
        LatLng latLng = new LatLng(lat, lng);
        MarkerOptions options = new MarkerOptions().position(latLng);
        options.title(MARKER_GPICACT);
        options.icon(BitmapDescriptorFactory.defaultMarker(
                BitmapDescriptorFactory.HUE_AZURE));

        Marker marker = mGoogleMap.addMarker(options);
        mGPicActMarkers.put(marker, gPicAct);
    }

    public void addGpicEntMarker(Gpic_Entry gPicEnt){
        double lat = Double.parseDouble(gPicEnt.getEntryLatitude());
        double lng = Double.parseDouble(gPicEnt.getEntryLongitude());
        LatLng latLng = new LatLng(lat, lng);
        MarkerOptions options = new MarkerOptions().position(latLng);
        options.title(MARKER_GPICENT);
        switch (Integer.parseInt(gPicEnt.getEntryType())){
            case 1:
                options.icon(BitmapDescriptorFactory.fromBitmap(
                        BitmapFactory.decodeResource(getResources(),
                                R.drawable.ic_photo_marker)));
                break;
            case 2:
                options.icon(BitmapDescriptorFactory.fromBitmap(
                        BitmapFactory.decodeResource(getResources(),
                                R.drawable.ic_audio_marker)));
                break;
            case 3:
                options.icon(BitmapDescriptorFactory.fromBitmap(
                        BitmapFactory.decodeResource(getResources(),
                                R.drawable.ic_text_marker)));
                break;
            case 4:
                options.icon(BitmapDescriptorFactory.fromBitmap(
                        BitmapFactory.decodeResource(getResources(),
                                R.drawable.ic_tag_marker)));
                break;
        }

        Marker marker = mGoogleMap.addMarker(options);
        mGPicEntMarkers.put(marker, gPicEnt);
    }

    public void removeGPicActMarker(Gpic_Activity gPicAct){
        Marker marker = getMarkerFromGPicAct(gPicAct);
        mGPicActMarkers.remove(marker);
        marker.remove();
    }

    private Marker getMarkerFromGPicAct(Gpic_Activity gPicAct){
        for (Map.Entry<Marker, Gpic_Activity> entry : mGPicActMarkers.entrySet()){
            if(entry.getValue().getName().equals(gPicAct.getName())){
                return entry.getKey();
            }
        }
        return null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mListener.onMapReady();
        mGoogleMap.setMapType(MAP_TYPES[curMapTypeIndex]);
        mGoogleMap.setTrafficEnabled(true);
        mGoogleMap.getUiSettings().setZoomControlsEnabled( true );

        mGoogleApiClient.connect();
        initListeners();
    }

    private void initListeners() {
        mGoogleMap.setOnMarkerClickListener(this);
        mGoogleMap.setOnMapLongClickListener(this);
        mGoogleMap.setOnMapClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mCurrentLocation = LocationServices
                .FusedLocationApi
                .getLastLocation(mGoogleApiClient);


        if(mCurrentLocation != null) initCamera(mCurrentLocation);

        if(waitingForLocationUpdate) startLocationUpdates();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private boolean waitingForLocationUpdate = false;
    public void startLocationUpdates() {
        if(!mGoogleApiClient.isConnected()){
            mGoogleApiClient.connect();
            waitingForLocationUpdate = true;
        }
        else{
            createLocationRequest();
            if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
            waitingForLocationUpdate = false;
        }

    }

    public void stopLocationUpdates(){
        if(mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss",Locale.US);
        String mLastUpdateTime  = sdf.format(new Date());
        addTrackingStep(mLastUpdateTime, location);
        if(initCam) {
            moveCamera(location);
        }
        else{
            initCamera(location);
        }
    }

    private boolean initCam = false;
    private void initCamera(Location location) {
        initCam = true;
        CameraPosition position = CameraPosition.builder()
                .target(new LatLng(location.getLatitude(),
                        location.getLongitude()))
                .zoom(14f)
                .bearing(0.0f)
                .tilt(0.0f)
                .build();

        mGoogleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(position), null);
    }

    private void moveCamera(Location location) {
        CameraPosition position = CameraPosition.builder()
                .target(new LatLng(location.getLatitude(),
                        location.getLongitude()))
                .zoom(mGoogleMap.getCameraPosition().zoom)
                .build();

        mGoogleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(position), null);
    }

    //region PATHING
    private void addTrackingStep(String time, Location location) {
        if(distancetoLast(location)){
            double lat = location.getLatitude();
            double lng = location.getLongitude();
            gPicPathing.addStep(time, Double.toString(lat), Double.toString(lng));
            PolylineOptions options = new PolylineOptions();
            options.color(Color.parseColor("#000000"));
            options.width(15);
            options.visible(true);
            if(mPolyline != null){
                options.addAll(mPolyline.getPoints());
                mPolyline.remove();
            }
            options.add(new LatLng(lat, lng));
            mPolyline = mGoogleMap.addPolyline(options);
            mListener.onAddStep(time,""+lat+","+lng);
        }
    }

    private boolean distancetoLast(Location location) {
        if(mPolyline != null) {
            Location lastLocation = new Location("");
            LatLng latLng = mPolyline.getPoints().get(mPolyline.getPoints().size() - 1);
            lastLocation.setLatitude(latLng.latitude);
            lastLocation.setLongitude(latLng.longitude);
            float distance = lastLocation.distanceTo(location);
            return distance > 3;
        }
        else{
            return true;
        }
    }
    //endregion

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("asd","ConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d("asd","ConnectionFailed");
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if(mCircle!=null && !workState){
            mCircle.remove();
        }
    }

    @Override
    public void onMapLongClick(final LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        switch (marker.getTitle()){
            case MARKER_GPICACT:
                Gpic_Activity gPicAct = mGPicActMarkers.get(marker);
                double lat = Double.parseDouble(gPicAct.getLatitude());
                double lng = Double.parseDouble(gPicAct.getLongitude());
                Location location = new Location("");
                location.setLatitude(lat);
                location.setLongitude(lng);
                LatLng latLng = new LatLng(lat, lng);

                moveToMarker(location);

                mRadius = Integer.parseInt(gPicAct.getRadius());
                drawCircle(marker.getPosition());
                break;
            case MARKER_GPICENT:
                Gpic_Entry gPicEnt = mGPicEntMarkers.get(marker);
                switch (Integer.parseInt(gPicEnt.getEntryType())){
                    case 1:
                        EntryPhoto_Dialog pDialog =
                                EntryPhoto_Dialog.newInstance(gPicEnt,true);
                        pDialog.setListener((Student_Homepage)getActivity());
                        pDialog.show(getFragmentManager(),"Photo Entry Dialog");
                        break;
                    case 2:
                        EntryAudio_Dialog aDialog =
                                EntryAudio_Dialog.newInstance(gPicEnt,true);
                        aDialog.setListener((Student_Homepage)getActivity());
                        aDialog.show(getFragmentManager(),"Audio Entry Dialog");
                        break;
                    case 3:
                        EntryText_Dialog tDialog =
                                EntryText_Dialog.newInstance(gPicEnt,true);
                        tDialog.setListener((Student_Homepage)getActivity());
                        tDialog.show(getFragmentManager(),"Text Entry Dialog");
                        break;
                    case 4:
                        break;
                }
            break;
        }

        return true;
    }

    public void onMarkerClick(Gpic_Activity gPicAct) {
        onMarkerClick(getMarkerFromGPicAct(gPicAct));
    }

    private void drawCircle( LatLng location ) {
        if(mCircle!=null) mCircle.remove();
        CircleOptions options = new CircleOptions();
        options.center(location);
        //Radius in meters
        options.radius(mRadius);
        options.fillColor( ContextCompat.getColor(getActivity(), R.color.creAct_fillColor ));
        options.strokeColor(ContextCompat.getColor(getActivity(), R.color.creAct_strokeColor ));
        options.strokeWidth(3);
        mCircle = mGoogleMap.addCircle(options);
    }

    private void moveToMarker(Location location) {
        CameraPosition position = CameraPosition.builder()
                .target(new LatLng(location.getLatitude(),
                        location.getLongitude()))
                .zoom(14f)
                .bearing(0.0f)
                .tilt(0.0f)
                .build();

        mGoogleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(position), null);
    }

    public void removeGPicEntMarker(Gpic_Entry gPicEnt){
        Marker marker = getMarkerFromGPicEnt(gPicEnt);
        mGPicActMarkers.remove(marker);
        marker.remove();
    }

    private Marker getMarkerFromGPicEnt(Gpic_Entry gPicEnt){
        for (Map.Entry<Marker, Gpic_Entry> entry : mGPicEntMarkers.entrySet()){
            if(entry.getValue().getEntryKey().equals(gPicEnt.getEntryKey())){
                return entry.getKey();
            }
        }
        return null;
    }

    public LatLng getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }
        Location location = LocationServices
                .FusedLocationApi
                .getLastLocation(mGoogleApiClient);
        if(location != null)
        return new LatLng(location.getLatitude(),location.getLongitude());
        else return new LatLng(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());
    }

    public void setWorkState(boolean workState, Gpic_Activity gPicAct){
        this.workState = workState;
        mRadius = Integer.parseInt(gPicAct.getRadius());
        if(workState){
            drawCircle(getMarkerFromGPicAct(gPicAct).getPosition());
        }else{
            mCircle.remove();
        }

        for (Map.Entry<Marker, Gpic_Activity> entry : mGPicActMarkers.entrySet()){
            Marker marker = entry.getKey();
            if(workState){
                marker.setVisible(false);
            }else {
                marker.setVisible(true);
            }
        }
    }
}
