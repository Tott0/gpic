package com.example.totto.proyecto_movil.Dialogs;

import com.example.totto.proyecto_movil.Models.Gpic_Entry;

/**
 * Created by Totto on 10/05/2016.
 */
public class EntryInterface {
    public interface EntryDialogsListener{
        public void onDeleteEntry(Gpic_Entry gPicEnt);

    }
}
