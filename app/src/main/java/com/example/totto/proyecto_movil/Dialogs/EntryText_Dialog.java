package com.example.totto.proyecto_movil.Dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.totto.proyecto_movil.Models.Gpic_Entry;
import com.example.totto.proyecto_movil.R;
import com.example.totto.proyecto_movil.StaticMethods;

public class EntryText_Dialog extends DialogFragment {

    private EditText mEntryText;
    private Gpic_Entry gPicEnt;
    private boolean deletable=false;

    EntryInterface.EntryDialogsListener mListener;

    public void setListener(EntryInterface.EntryDialogsListener listener) {
        mListener = listener;
    }

    public EntryText_Dialog(){

    }

    public static EntryText_Dialog newInstance(Gpic_Entry gPicEnt, boolean deletable){
        EntryText_Dialog f = new EntryText_Dialog();
        Bundle args = new Bundle();
        args.putSerializable("gPicEnt", gPicEnt);
        args.putBoolean("deletable",deletable);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gPicEnt = (Gpic_Entry) getArguments().getSerializable("gPicEnt");
        deletable = getArguments().getBoolean("deletable");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.entrytext_diafrag, null);

        mEntryText = (EditText)v.findViewById(R.id.tvEntryText);
        mEntryText.setText(gPicEnt.getEntryValue());

        Button btnDelete = (Button)v.findViewById(R.id.btnDeleteEntryText);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                builder.setMessage("Delete Entry?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mListener.onDeleteEntry(gPicEnt);
                                EntryText_Dialog.this.dismiss();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).show();
            }
        });

        if(!deletable){
            btnDelete.setVisibility(View.GONE);
        }

        builder.setView(v)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog dialog = builder.create();
        Window window = dialog.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setGravity(Gravity.CENTER);
        return dialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }
}
