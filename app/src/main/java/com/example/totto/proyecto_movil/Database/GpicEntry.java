package com.example.totto.proyecto_movil.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by Totto on 05/05/2016.
 */
public final class GpicEntry {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public GpicEntry() {}

    /* Inner class that defines the table contents */
    public static abstract class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_ENTRY_LATITUDE  = "entryLatitude";
        public static final String COLUMN_NAME_ENTRY_LONGITUDE = "entryLongitude";
        public static final String COLUMN_NAME_ENTRY_TYPE      = "entryType";
        public static final String COLUMN_NAME_ENTRY_Value     = "entryValue";
        public static final String COLUMN_NAME_ENTRY_Key       = "entryKey";
        public static final String COLUMN_NAME_ENTRY_Student   = "entryStudent";
        public static final String COLUMN_NAME_ENTRY_Activity  = "entryActivity";
    }




}




