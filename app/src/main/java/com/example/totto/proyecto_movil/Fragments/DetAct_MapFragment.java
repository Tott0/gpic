package com.example.totto.proyecto_movil.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.example.totto.proyecto_movil.Dialogs.EntryAudio_Dialog;
import com.example.totto.proyecto_movil.Dialogs.EntryPhoto_Dialog;
import com.example.totto.proyecto_movil.Dialogs.EntryText_Dialog;
import com.example.totto.proyecto_movil.Dialogs.Teacher_RadiusInput_Dialog;
import com.example.totto.proyecto_movil.Models.Gpic_Activity;
import com.example.totto.proyecto_movil.Models.Gpic_Entry;
import com.example.totto.proyecto_movil.Models.Gpic_Pathing;
import com.example.totto.proyecto_movil.R;
import com.example.totto.proyecto_movil.Student_Homepage;
import com.example.totto.proyecto_movil.Teacher_DetailAct;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Totto on 30/04/2016.
 */
public class DetAct_MapFragment extends MapFragment implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener, OnMapReadyCallback {

    private GoogleApiClient mGoogleApiClient;
    private final int[] MAP_TYPES = {GoogleMap.MAP_TYPE_SATELLITE,
            GoogleMap.MAP_TYPE_NORMAL,
            GoogleMap.MAP_TYPE_HYBRID,
            GoogleMap.MAP_TYPE_TERRAIN,
            GoogleMap.MAP_TYPE_NONE};

    private int curMapTypeIndex = 2;

    private Gpic_Activity mGPicAct;
    private LatLng mLocation;


    private Polyline mPolyline;
    private Map<Marker, Gpic_Entry> mGPicEntMarkers = new HashMap<>();

    private final String LOG_TAG        = "asdDetMapFragment";
    private final String MARKER_GPICACT = "GpicAct";
    private final String MARKER_GPICENT = "GpicEnt";

    DetActMapFragmentListener mListener;
    public interface DetActMapFragmentListener {
        public void mapReady();
    }



    public void setListener(DetActMapFragmentListener listener) {
        mListener = listener;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        this.getMapAsync(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("asd","ConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Log.d("asd","ConnectionFailed");

    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    private GoogleMap mGoogleMap;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        initListeners();
        mListener.mapReady();
    }

    private void initListeners() {
        mGoogleMap.setOnMarkerClickListener(this);
        mGoogleMap.setOnInfoWindowClickListener(this);
        mGoogleMap.setOnMapClickListener(this);
    }




    public void initMapFragment( Gpic_Activity gPicAct ) {
        mGPicAct = gPicAct;
        Location location = new Location("");
        location.setLatitude  (Double.parseDouble(mGPicAct.getLatitude()));
        location.setLongitude(Double.parseDouble(mGPicAct.getLongitude()));
        LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());
        mLocation = latLng;

        moveToActivity();
        addActivityMarker();
    }

    private void moveToActivity() {
        CameraPosition position = CameraPosition.builder()
                .target(mLocation)
                .zoom( 14f )
                .bearing( 0.0f )
                .tilt( 0.0f )
                .build();

        mGoogleMap.animateCamera( CameraUpdateFactory
                .newCameraPosition( position ), null );

        mGoogleMap.setMapType( MAP_TYPES[curMapTypeIndex] );
        mGoogleMap.setTrafficEnabled( true );
        mGoogleMap.getUiSettings().setZoomControlsEnabled( true );
    }

    private void addActivityMarker() {
        MarkerOptions options = new MarkerOptions().position( mLocation );
        options.title( MARKER_GPICACT );
        options.icon( BitmapDescriptorFactory.defaultMarker(
                BitmapDescriptorFactory.HUE_AZURE) );
        mGoogleMap.clear();
        mGoogleMap.addMarker(options);
        drawCircle();
    }

    private void drawCircle() {
        CircleOptions options = new CircleOptions();
        options.center(mLocation);
        options.radius(Double.parseDouble(mGPicAct.getRadius()));
        options.fillColor( ContextCompat.getColor(getActivity(), R.color.detAct_fillColor ));
        options.strokeColor(ContextCompat.getColor(getActivity(), R.color.creAct_strokeColor));
        options.strokeWidth(3);
        mGoogleMap.addCircle(options);
    }

    public void addGpicEntMarkers(List<Gpic_Entry> gPicEnts){
        for (Gpic_Entry gPicEnt : gPicEnts) {
            double lat = Double.parseDouble(gPicEnt.getEntryLatitude());
            double lng = Double.parseDouble(gPicEnt.getEntryLongitude());
            LatLng latLng = new LatLng(lat, lng);
            MarkerOptions options = new MarkerOptions().position(latLng);
            options.title(MARKER_GPICENT);
            switch (Integer.parseInt(gPicEnt.getEntryType())) {
                case 1:
                    options.icon(BitmapDescriptorFactory.fromBitmap(
                            BitmapFactory.decodeResource(getResources(),
                                    R.drawable.ic_photo_marker)));
                    break;
                case 2:
                    options.icon(BitmapDescriptorFactory.fromBitmap(
                            BitmapFactory.decodeResource(getResources(),
                                    R.drawable.ic_audio_marker)));
                    break;
                case 3:
                    options.icon(BitmapDescriptorFactory.fromBitmap(
                            BitmapFactory.decodeResource(getResources(),
                                    R.drawable.ic_text_marker)));
                    break;
                case 4:
                    options.icon(BitmapDescriptorFactory.fromBitmap(
                            BitmapFactory.decodeResource(getResources(),
                                    R.drawable.ic_tag_marker)));
                    break;
            }

            Marker marker = mGoogleMap.addMarker(options);
            mGPicEntMarkers.put(marker, gPicEnt);
        }
    }

    public void addPathing(Gpic_Pathing gPicPath){
        PolylineOptions options = new PolylineOptions();
        options.color(Color.parseColor("#000000"));
        options.width(15);
        options.visible(true);
        for (int i = 0; i < gPicPath.getTimes().size(); i++) {
                double lat  = Double.parseDouble(gPicPath.getLatitudes().get(i));
                double lng  = Double.parseDouble(gPicPath.getLongitudes().get(i));
                options.add(new LatLng(lat, lng));
            }
        mPolyline = mGoogleMap.addPolyline(options);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if(marker.getTitle().equals(MARKER_GPICENT)){
            Gpic_Entry gPicEnt = mGPicEntMarkers.get(marker);
            switch (Integer.parseInt(gPicEnt.getEntryType())){
                case 1:
                    EntryPhoto_Dialog pDialog =
                            EntryPhoto_Dialog.newInstance(gPicEnt,true);
                    pDialog.show(getFragmentManager(),"Photo Entry Dialog");
                    break;
                case 2:
                    EntryAudio_Dialog aDialog =
                            EntryAudio_Dialog.newInstance(gPicEnt,true);
                    aDialog.show(getFragmentManager(),"Audio Entry Dialog");
                    break;
                case 3:
                    EntryText_Dialog mDialog =
                            EntryText_Dialog.newInstance(gPicEnt,false);
                    mDialog.show(getFragmentManager(),"Text Entry Dialog");
                    break;
                case 4:
                    break;
            }
        }

        return true;
    }



}
