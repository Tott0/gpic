package com.example.totto.proyecto_movil.Dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.totto.proyecto_movil.Models.Gpic_Entry;
import com.example.totto.proyecto_movil.R;

import java.io.InputStream;

public class EntryPhoto_Dialog extends DialogFragment {

    private ImageView mEntryPhoto;
    private Gpic_Entry gPicEnt;
    private boolean deletable=false;
    private ProgressBar mProgressBar;

    EntryInterface.EntryDialogsListener mListener;

    public void setListener(EntryInterface.EntryDialogsListener listener) {
        mListener = listener;
    }

    public EntryPhoto_Dialog(){

    }

    public static EntryPhoto_Dialog newInstance(Gpic_Entry gPicEnt, boolean deletable){
        EntryPhoto_Dialog f = new EntryPhoto_Dialog();
        Bundle args = new Bundle();
        args.putSerializable("gPicEnt", gPicEnt);
        args.putBoolean("deletable",deletable);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gPicEnt = (Gpic_Entry) getArguments().getSerializable("gPicEnt");
        deletable = getArguments().getBoolean("deletable");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.entryphoto_diafrag, null);

        mEntryPhoto = (ImageView) v.findViewById(R.id.ivEntryPhoto);
        mProgressBar = (ProgressBar) v.findViewById(R.id.pbEntryPhoto);
        new DownloadImageTask().execute(gPicEnt.getEntryValue());

        Button btnDelete = (Button)v.findViewById(R.id.btnDeleteEntryPhoto);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                builder.setMessage("Delete Entry?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mListener.onDeleteEntry(gPicEnt);
                                EntryPhoto_Dialog.this.dismiss();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).show();
            }
        });

        if(!deletable){
            btnDelete.setVisibility(View.GONE);
        }

        builder.setView(v)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog dialog = builder.create();
        Window window = dialog.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setGravity(Gravity.CENTER);
        return dialog;
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        public DownloadImageTask() {
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            mEntryPhoto.setImageBitmap(result);
            mProgressBar.setVisibility(View.GONE);
        }
    }
}
