package com.example.totto.proyecto_movil;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Totto on 02/05/2016.
 */
public class LoginSharedPreference {
    static final String PREF_USER_NAME= "username";
    static final String PREF_USER_TYPE= "usertype";
    static final String PREF_NETWORK_SETTING = "networkSetting";

    private static SharedPreferences getSharedPreferences(Context ctx){
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setUserName(Context ctx, String userName){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.apply();
    }

    public static void setUserType(Context ctx, String userType){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_TYPE, userType);
        editor.apply();
    }

    public static void setNetworkSetting(Context ctx, String networkSetting){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_NETWORK_SETTING, networkSetting);
        editor.apply();
    }

    public static String getUserName(Context ctx){
        return getSharedPreferences(ctx).getString(PREF_USER_NAME,"");
    }

    public static String getUserType(Context ctx){
        return getSharedPreferences(ctx).getString(PREF_USER_TYPE,"");
    }

    public static String getNetworkSetting(Context ctx){
        return getSharedPreferences(ctx).getString(PREF_NETWORK_SETTING,"0");
    }

    public static void clear(Context ctx){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.clear();
        editor.apply();
    }
}

