package com.example.totto.proyecto_movil.Models;

/**
 * Created by Totto on 29/04/2016.
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Gpic_Pathing implements Serializable {

    List<String> times;
    List<String> latitudes;
    List<String> longitudes;

    public Gpic_Pathing(){
        times      = new ArrayList<String>();
        latitudes  = new ArrayList<String>();
        longitudes = new ArrayList<String>();
    }

    public Gpic_Pathing(List<String> times, List<String> latitudes, List<String> longitudes) {
        this.times = times;
        this.latitudes = latitudes;
        this.longitudes = longitudes;
    }

    public List<String> getLatitudes() {
        return latitudes;
    }

    public void setLatitudes(List<String> latitudes) {
        this.latitudes = latitudes;
    }

    public List<String> getLongitudes() {
        return longitudes;
    }

    public void setLongitudes(List<String> longitudes) {
        this.longitudes = longitudes;
    }

    public List<String> getTimes() {
        return times;
    }

    public void setLatTimes(List<String> times) {
        this.times = times;
    }




    public void addStep(String time, String lat, String lng){
        times.add(time);
        latitudes.add(lat);
        longitudes.add(lng);
    }

}
