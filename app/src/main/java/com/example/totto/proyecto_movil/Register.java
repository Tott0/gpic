package com.example.totto.proyecto_movil;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.totto.proyecto_movil.Models.Gpic_User;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class Register extends AppCompatActivity {

    private EditText tvUserName;
    private EditText tvName;
    private EditText tvPass;
    private EditText tvCPass;

    private boolean match;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        tvUserName = (EditText)findViewById(R.id.tvRegisterUsername);
        tvName     = (EditText)findViewById(R.id.tvRegisterName);
        tvPass     = (EditText)findViewById(R.id.tvRegisterPass);
        tvCPass    = (EditText)findViewById(R.id.tvRegisterConfirmPass);

        tvCPass.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    match = tvPass.getText().toString().equals(
                            tvCPass.getText().toString());
                    if(!match){
                        tvCPass.setError("Passwords no son iguales");
                    }
                }
            }
        });
        tvCPass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    StaticMethods.EditTextDoneAction(v, Register.this);
                }
                return false;
            }
        });
    }

    public void btnRegisterOnClick(View view) {
        final String userName = tvUserName.getText().toString();
        final String name = tvName.getText().toString();
        final String pass = tvPass.getText().toString();
        final String cPass = tvCPass.getText().toString();

        if ("".equals(name) || "".equals(userName) || "".equals(pass) ||
                "".equals(cPass)) {

            Toast.makeText(this, "Complete todos los campos", Toast.LENGTH_SHORT).show();

            if ("".equals(name)) {
                tvName.setError("Nombre no puede estar vacío");
            }
            if ("".equals(userName)) {
                tvUserName.setError("Username no puede estar vacío");
            }
            if ("".equals(pass)) {
                tvPass.setError("Password no puede estar vacío");
            }
            if ("".equals(cPass)) {
                tvCPass.setError("Password no puede estar vacío");
            }
        } else {
            if(match) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Register?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                attemptCreate(userName, name, pass);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).show();

            }
            else{
                Toast.makeText(Register.this,"Passwords no coinciden",Toast.LENGTH_LONG).show();
            }
        }
    }

    private void attemptCreate(final String userName, final String name, final String pass) {
        final Firebase regFirebase = new Firebase(MainActivity.fbRoot+"Users/");
        regFirebase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.child(userName).exists()){
                    tvUserName.setError("Username repetido");
                    Toast.makeText(Register.this,"Username ya existe",Toast.LENGTH_LONG).show();
                }else{
                    Gpic_User user = new Gpic_User();
                    user.setUserID(userName);
                    user.setUserName(name);
                    user.setUserPass(StaticMethods.MD5(pass));
                    user.setUserType("0");

                    regFirebase.child(userName).setValue(user);
                    Intent i = new Intent(Register.this,Login.class);
                    startActivity(i);
                    Register.this.finish();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
}
