package com.example.totto.proyecto_movil.ApiImgur;

/**
 * Created by com.example.totto.proyecto_movil on 2/23/15.
 */
public class Constants {
    /*
      Logging flag
     */
    public static final boolean LOGGING = false;

    /*
      Your imgur client id. You need this to upload to imgur.

      More here: https://api.imgur.com/
     */
    public static final String MY_IMGUR_CLIENT_ID = "c5cbdd0a6866b5b";
    public static final String MY_IMGUR_CLIENT_SECRET = "f2989ce013dee8a2aa0ab2675819f9e5b1d4afee";

    /*
      Redirect URL for android.
     */
    public static final String MY_IMGUR_REDIRECT_URL = "http://android";

    /*
      Client Auth
     */
    public static String getClientAuth() {
        return "Client-ID " + MY_IMGUR_CLIENT_ID;
    }

}
