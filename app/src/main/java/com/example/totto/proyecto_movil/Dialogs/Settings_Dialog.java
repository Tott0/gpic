package com.example.totto.proyecto_movil.Dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;

import com.example.totto.proyecto_movil.Login;
import com.example.totto.proyecto_movil.LoginSharedPreference;
import com.example.totto.proyecto_movil.Models.Gpic_Entry;
import com.example.totto.proyecto_movil.R;

import java.io.InputStream;

public class Settings_Dialog extends DialogFragment {

    private Switch swSettings;
    private int netSettings;

    private SettingsListener mListener;

    public interface SettingsListener{
        public void SettingsClosed();
    }

    public void setListener(SettingsListener mListener) {
        this.mListener = mListener;
    }

    public Settings_Dialog(){

    }

    public static Settings_Dialog newInstance(){
        Settings_Dialog f = new Settings_Dialog();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        netSettings = Integer.parseInt(LoginSharedPreference.getNetworkSetting(getActivity()));
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.settings_diafrag, null);

        swSettings = (Switch) v.findViewById(R.id.swSettings);
        if(netSettings == 1){
            swSettings.setChecked(true);
        }
        else{
            swSettings.setChecked(false);
        }
        swSettings.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int opt = 0;
                if(isChecked) opt = 1;
                LoginSharedPreference.setNetworkSetting(getActivity(),""+opt);
            }
        });

        builder.setView(v)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog dialog = builder.create();
        Window window = dialog.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setGravity(Gravity.CENTER);
        return dialog;
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        mListener.SettingsClosed();
    }

}
