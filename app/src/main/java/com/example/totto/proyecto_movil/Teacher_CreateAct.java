package com.example.totto.proyecto_movil;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.totto.proyecto_movil.Fragments.CreAct_MapFragment;
import com.example.totto.proyecto_movil.Models.Gpic_Activity;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Teacher_CreateAct extends AppCompatActivity implements CreAct_MapFragment.CreActMapFragmentListener {

    private Gpic_Activity gpicActivity;
    private boolean location = false;
    private boolean name  = false;
    private boolean c1 = false;
    private boolean c2 = false;
    private boolean check = false;

    private TextView tvname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teacher__createact);

        gpicActivity = new Gpic_Activity();
        gpicActivity.setTeacherID(getIntent().getStringExtra("teacherID"));
        gpicActivity.setDescription("");

        CreAct_MapFragment mapFragment = (CreAct_MapFragment)getFragmentManager().
                findFragmentById(R.id.createMap);
        mapFragment.setListener(this);

        tvname = (TextView)findViewById(R.id.thc_tvName);
        if (tvname != null) {
            tvname.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus) {
                        String tName = ((TextView) v).getText().toString();
                        gpicActivity.setName(tName);
                        if ("".equals(tName)) {
                            name = false;
                            check = false;
                        } else {
                            name = true;
                            checkExists();
                        }
                    }
                }
            });
        }else{
            Log.e("asd","tvName is null");
        }

        TextView tvdesc = (TextView)findViewById(R.id.thc_tvDesc);
        if (tvdesc != null) {
            tvdesc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    gpicActivity.setDescription(((TextView)v).getText().toString());
                }
            });
            tvdesc.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if(actionId== EditorInfo.IME_ACTION_DONE){
                        //Clear focus here from edittext
                        StaticMethods.EditTextDoneAction(v, Teacher_CreateAct.this);
                    }
                    return false;
                }
            });
        }
    }

    @Override
    public void RadiusChange(int radius) {
        gpicActivity.setRadius(Integer.toString(radius));
    }

    @Override
    public void LocationChange(double lat, double lng) {
        gpicActivity.setLatitude(Double.toString(lat));
        gpicActivity.setLongitude(Double.toString(lng));
        if(!this.location) this.location= true;

    }

    public boolean isComplete(){
        return location && name;
    }

    public void btnCreateOnClick(View view) {
        checkExists();
        if(!isComplete()){
            if(!location){
                Toast.makeText(this,"Seleccione ubicación en el mapa",
                        Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(this,"Complete el campo de nombre",
                        Toast.LENGTH_SHORT).show();

                TextView tvname = (TextView)findViewById(R.id.thc_tvName);
                tvname.setError("Nombre no puede estar vacío");
            }
        }
        else{
            if(c1 || c2 || !check){
                if(!check){
                    Toast.makeText(this, "Verificando Validez de Nombre",
                            Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(this, "Nombre seleccionado ya existe",
                            Toast.LENGTH_SHORT).show();
                }
            }
            else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Crear actividad?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent data = new Intent();
                                data.putExtra("data", gpicActivity);
                                setResult(RESULT_OK, data);
                                finish();

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).show();
            }
        }
    }


    private boolean checkExists() {
        Firebase checkFirebaseRef = new Firebase(MainActivity.fbRoot+
                "Active_Activities/"+gpicActivity.getName()+"/");
        checkFirebaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                c1 = dataSnapshot.exists();
                if(c1) {
                    tvname.setError("Nombre ya existe");
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }

        });

        checkFirebaseRef = new Firebase(MainActivity.fbRoot+
                "Closed_Activities/"+gpicActivity.getName()+"/");

        checkFirebaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                c2 = dataSnapshot.exists();
                if(c2){
                    tvname.setError("Nombre ya existe");
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }

        });
        check = true;
        return c1 || c2;
    }

    public void thc_btnBackOnClick(View view) {
        finish();
    }
}
