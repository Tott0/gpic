package com.example.totto.proyecto_movil.RecycleAdapters; /**
 * Created by Laboratorio on 18/04/2016.
 */
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.totto.proyecto_movil.Models.Gpic_Activity;
import com.example.totto.proyecto_movil.Models.Gpic_Submit;
import com.example.totto.proyecto_movil.R;

import java.util.Collections;
import java.util.List;

public class th_DetailAdapter extends RecyclerView.Adapter<th_DetailAdapter.MyViewHolder>{

    private final Context context;
    private LayoutInflater inflater;
    private List<Gpic_Submit> data = Collections.emptyList();
    private th_DetailRecListener mRecyclerClickListner;

    public th_DetailAdapter(Context context, List<Gpic_Submit> data){
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.th_detail_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Gpic_Submit gPicSub = data.get(position);
        holder.tv1.setText(gPicSub.getStudentID());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public void addItem(int position, Gpic_Submit data) {
        this.data.add(position, data);
        notifyItemInserted(position);
    }

    public void editItem(int position, Gpic_Submit data) {
        this.data.set(position, data);
        notifyItemChanged(position);
    }

    public void removeItem(int position) {
        this.data.remove(position);
        notifyItemRemoved(position);
    }

    public Gpic_Submit getItem(int position){return this.data.get(position);

    }

    public void clearData() {
        int size = this.data.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.data.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void setRecyclerClickListner(th_DetailRecListener recyclerClickListner){
        mRecyclerClickListner = recyclerClickListner;
    }


    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv1;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tv1 = (TextView) itemView.findViewById(R.id.tvDetField1);
        }

        @Override
        public void onClick(View v) {
            if (mRecyclerClickListner != null) {
                mRecyclerClickListner.itemClick(v, getAdapterPosition());
            }
        }
    }

    public interface th_DetailRecListener
    {
        public void itemClick(View view, int position);
    }
}