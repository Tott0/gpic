package com.example.totto.proyecto_movil;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.totto.proyecto_movil.Models.Gpic_User;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    public final static String fbRoot = ("https://gpic.firebaseio.com/");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        Firebase.setAndroidContext(this);
        Intent i = null;
        String savedUserID = LoginSharedPreference.getUserName(MainActivity.this);
        String savedUserType = LoginSharedPreference.getUserType(MainActivity.this);
        if(savedUserID.length() == 0){
            i = new Intent(this, Login.class);
        }else{
            int type = Integer.parseInt(savedUserType);
            switch (type){
                case 0:
                    i = new Intent(this, Student_Homepage.class);
                    i.putExtra("userName",savedUserID);
                    break;
                case 1:
                    i = new Intent(this, Teacher_Homepage.class);
                    i.putExtra("userName",savedUserID);
                    break;
            }
        }
        startActivity(i);
        finish();
    }
}
