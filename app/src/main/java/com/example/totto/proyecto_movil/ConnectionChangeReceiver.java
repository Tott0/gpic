package com.example.totto.proyecto_movil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.example.totto.proyecto_movil.ApiImgur.utils.NetworkUtils;

import java.util.ArrayList;

/**
 * Created by Totto on 10/05/2016.
 */
public class ConnectionChangeReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(final Context context, final Intent intent) {
            int status = StaticMethods.getConnectivityStatusString(context);
            if (status != StaticMethods.NETWORK_STATUS_NOT_CONNECTED) {
                Toast.makeText(context, "Connected", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(context, "No Connection", Toast.LENGTH_SHORT).show();
            }
            Intent i = new Intent("CONNETIVITY_CHANGE");
            i.putExtra("networkStatus", status);
            LocalBroadcastManager.getInstance(context).sendBroadcast(i);
    }
}
