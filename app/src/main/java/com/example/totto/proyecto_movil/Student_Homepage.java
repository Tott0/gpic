package com.example.totto.proyecto_movil;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.example.totto.proyecto_movil.ApiImgur.imgurmodel.ImageResponse;
import com.example.totto.proyecto_movil.ApiImgur.imgurmodel.Upload;
import com.example.totto.proyecto_movil.ApiImgur.services.UploadService;
import com.example.totto.proyecto_movil.DataUpload.AudioRecording;
import com.example.totto.proyecto_movil.DataUpload.AudioUpload;
import com.example.totto.proyecto_movil.Database.EntryDbHelper;
import com.example.totto.proyecto_movil.Database.GpicEntry;
import com.example.totto.proyecto_movil.Dialogs.EntryInterface;
import com.example.totto.proyecto_movil.Dialogs.EntryPhoto_Dialog;
import com.example.totto.proyecto_movil.Dialogs.Settings_Dialog;
import com.example.totto.proyecto_movil.Dialogs.Student_TextEntry_Dialog;
import com.example.totto.proyecto_movil.Dialogs.Teacher_RadiusInput_Dialog;
import com.example.totto.proyecto_movil.Fragments.StHom_MapFragment;
import com.example.totto.proyecto_movil.Fragments.StHom_RecFragment;
import com.example.totto.proyecto_movil.Fragments.StHom_WorkFragment;
import com.example.totto.proyecto_movil.Models.Gpic_Activity;
import com.example.totto.proyecto_movil.Models.Gpic_Entry;
import com.example.totto.proyecto_movil.Models.Gpic_Pathing;
import com.example.totto.proyecto_movil.Models.Gpic_Submit;
import com.example.totto.proyecto_movil.Services.Student_HomLocationService;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Student_Homepage extends AppCompatActivity implements
        StHom_MapFragment.stHomMapFragmentListener,
        StHom_RecFragment.StHomRecFragmentListener,
        StHom_WorkFragment.StHomWorkFragmentListener,
        AudioRecording.AudioRecordingListener,
        Student_TextEntry_Dialog.TextEntryDialogListener,
        EntryInterface.EntryDialogsListener,
        Settings_Dialog.SettingsListener{


    Firebase myFirebaseRef;
    private List<Gpic_Activity> mData = new ArrayList<>();

    private String mStudentID;
    private StHom_MapFragment mapFragment;
    private StHom_RecFragment recFragment;
    private StHom_WorkFragment workFragment;

    private EntryDbHelper mDbHelper;
    private AudioRecording audioRecording;
    private AudioUpload audioUpload;

    private FloatingActionsMenu  fabMenu;
    private FloatingActionButton fabAudio;
    private ProgressDialog       mProgressDia;
    private Intent               mServiceIntent;

    private final String LOG_TAG = "asdStudentHomepage";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student__homepage);

        mDbHelper = new EntryDbHelper(this);

        mStudentID = LoginSharedPreference.getUserName(Student_Homepage.this);

        fabMenu = ((FloatingActionsMenu)findViewById(R.id.st_fabWorkMenu));
        assert fabMenu != null;
        fabMenu.setVisibility(View.GONE);

        fabAudio = (FloatingActionButton)findViewById(R.id.st_fabWorkAudio);
        audioRecording = new AudioRecording();
        audioRecording.setListener(this);
        setRecordFab();
        audioUpload = new AudioUpload();

        recFragment = StHom_RecFragment.newInstance();
        workFragment = StHom_WorkFragment.newInstance();
        setBottomFragment(false);

        mapFragment = (StHom_MapFragment) getFragmentManager().
                findFragmentById(R.id.studentMap);
        mapFragment.setListener(this);

        myFirebaseRef = new Firebase(MainActivity.fbRoot+"Active_Activities/");

        IntentFilter filter = new IntentFilter("GpsLocationUpdate");
        ResponseReceiver receiver = new ResponseReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
        LocalBroadcastManager.getInstance(this).registerReceiver(
                receiver, new IntentFilter("CONNETIVITY_CHANGE"));
        mServiceIntent = new Intent(this, Student_HomLocationService.class);
    }

    private boolean mLocationState=true; // false -> Rec Fragment
    private void setBottomFragment(boolean locationState) {
        if(locationState != mLocationState) {
            mLocationState = locationState;
            if(locationState){
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.st_fragment,workFragment).commit();
            }else{
                fabMenu.collapse();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.st_fragment,recFragment).commit();
            }
        }else {
            Log.d(LOG_TAG, "no fragment change");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.homepage_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.miLogout:
                StaticMethods.attemptLogout(Student_Homepage.this);
                return true;
            case R.id.miSettings:
                Settings_Dialog sDialog =
                        Settings_Dialog.newInstance();
                sDialog.setListener(this);
                sDialog.show(getFragmentManager(),"Settings Dialog");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void SettingsClosed(){
        checkUploadSubmit();
    }

    @Override
    protected void onDestroy() {
        Log.d(LOG_TAG,"onDestroy");
        super.onDestroy();
        if(mServiceIntent != null){
            stopService(mServiceIntent);
            mapFragment.stopLocationUpdates();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mReadyState >= 2 && !mWorkState)
        checkUploadSubmit();
    }

    private void setChildListener(){
        myFirebaseRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Gpic_Activity gPicAct = dataSnapshot.getValue(Gpic_Activity.class);
                mapFragment.addGPicActMarker(gPicAct);
                if(!mLocationState) recFragment.addGpicActItem(gPicAct);
                mData.add(gPicAct);
                updateService(1,gPicAct);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Gpic_Activity gPicAct = dataSnapshot.getValue(Gpic_Activity.class);
                mapFragment.removeGPicActMarker(gPicAct);
                int index = getIndexOf(gPicAct);
                if(!mLocationState) recFragment.removeGpicActItem(index);
                mData.remove(index);
                updateService(-1,gPicAct);

                //myFirebaseRef.child(gPicAct.getName()).setValue(gPicAct);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    private void updateService(int action, Gpic_Activity gPicAct) {
        mServiceIntent = new Intent(Student_Homepage.this, Student_HomLocationService.class);
        mServiceIntent.putExtra("action",action);
        mServiceIntent.putExtra("gPicAct",gPicAct);
        startService(mServiceIntent);
    }

    private boolean mDataLoaded = false;
    private void checkInitDataFinished(){
        myFirebaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mDataLoaded = true;
                mProgressDia.dismiss();
                startLocationService(true);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    private int getIndexOf(Gpic_Activity gPicAct){
        int i = 0;
        for (Gpic_Activity activity : mData){
            if(activity.getName().equals(gPicAct.getName())){
                return i;
            }
            else{
                i++;
            }
        }
        return -1;
    }

    private void startLocationService(boolean start){
        mServiceIntent = new Intent(Student_Homepage.this, Student_HomLocationService.class);
        mServiceIntent.putExtra("startLocationService",start);
        startService(mServiceIntent);
    }

    private int mReadyState = 0;
    @Override
    public void onMapReady() {
        Log.d(LOG_TAG,"map ready");
        mReadyState += 1;
        if((mReadyState == 2) && !mDataLoaded ){
            initSetup();
        }
    }
    @Override
    public void onRecFragmentReady() {
        Log.d(LOG_TAG,"rec ready");
        mReadyState += 1;
        if(mReadyState == 2 && !mDataLoaded){
            initSetup();
        }else {
            if (mReadyState >= 2){
                recFragment.setGPicActItems(mData);
            }
        }
    }

    private void initSetup(){
        if(StaticMethods.getConnectivityStatusString(this)!=StaticMethods.NETWORK_STATUS_NOT_CONNECTED){
            mProgressDia = ProgressDialog.show(Student_Homepage.this,
                    "Loading","Loading Ativity Data");
            setChildListener();
            checkInitDataFinished();
        }else{
            Log.e(LOG_TAG,"No Connection - init");
        }
    }

    @Override
    public void onRecGPicClick(Gpic_Activity gPicAct) {
        mapFragment.onMarkerClick(gPicAct);
    }

    private boolean mWorkState = false;
    @Override
    public void onToggle(boolean checkedState) {
        if(checkedState){
            StaticMethods.viewFadeIn(Student_Homepage.this, fabMenu);
            startLocationService(false);
            mapFragment.startLocationUpdates();
            clearPathing();
            mapFragment.setWorkState(true, workStateGPicAct);
            Log.d(LOG_TAG,"Starting data take for activity: "+workStateGPicAct.getName());
            mWorkState = true;
        }
        else{
            fabMenu.collapse();
            StaticMethods.viewFadeOut(Student_Homepage.this, fabMenu);
            startLocationService(true);
            mapFragment.stopLocationUpdates();
            mapFragment.setWorkState(false, workStateGPicAct);
            checkUploadSubmit();
            mWorkState = false;
        }
    }

    private Gpic_Activity workStateGPicAct;
    private class ResponseReceiver extends BroadcastReceiver{
        public ResponseReceiver(){

        }

        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()){
                case "GpsLocationUpdate":
                    workStateGPicAct = (Gpic_Activity)intent.getExtras().get("locationState");
                    Log.d(LOG_TAG,workStateGPicAct.getName());
                    boolean locationState = false;
                    if(workStateGPicAct != null) locationState = true;
                    setBottomFragment(locationState);
                    break;
                case "CONNETIVITY_CHANGE":
                    int status = intent.getIntExtra("networkStatus",0);
                    if(status != 0 && (mReadyState == 2 && !mDataLoaded)){
                        initSetup();
                    }
                    break;
            }
        }


    }

    //region Data Take
    public void st_fabOnClick(View view) {
        //TODO el tag pa la version premium
        fabMenu.collapse();
        switch (view.getId()){
            case R.id.st_fabWorkPhoto:
                //picture take is done
                stTakePhoto();
                break;
            case R.id.st_fabWorkText:
                //dialog to ask for text, izi
                stWriteText();
                break;
            case R.id.st_fabWorkTag:
                //where the tags at tho
                stWriteTag();
                break;
        }
    }

    //region Photo Take
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_TAKE_PHOTO:
                if(resultCode == RESULT_OK) {
                    Upload upload = new Upload();
                    File file = new File(mCurrentPhotoPath);
                    upload.image = file;
                    Log.d(LOG_TAG, upload.image.toString());
                    mProgressDia = ProgressDialog.show(Student_Homepage.this, "", "Uploading photo...", true);
                    new UploadService(Student_Homepage.this).Execute(upload, new UiCallback(file));
                    break;
                }else{
                    Log.d(LOG_TAG,"canceled photo");
                    File file = new File(mCurrentPhotoPath);
                    file.delete();
                }
        }
    }



    private String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    static final int REQUEST_TAKE_PHOTO = 1;

    public void stTakePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }
    private class UiCallback implements Callback<ImageResponse> {

        private File mPhoto;

        public UiCallback(File mPhoto){
            this.mPhoto = mPhoto;
        }
        @Override
        public void success(ImageResponse imageResponse, Response response) {
            LatLng latLng = mapFragment.getCurrentLocation();
            Gpic_Entry gPicEnt = new Gpic_Entry();
            gPicEnt.setEntryLatitude(Double.toString(latLng.latitude));
            gPicEnt.setEntryLongitude(Double.toString(latLng.longitude));
            gPicEnt.setEntryType("1");
            gPicEnt.setEntryValue(imageResponse.data.getLink());

            mDbHelper.addItem(gPicEnt,mStudentID,workStateGPicAct.getName());
            mapFragment.addGpicEntMarker(gPicEnt);
            boolean del = mPhoto.delete();
            mProgressDia.dismiss();

            //db.delete(GpicEntry.FeedEntry.TABLE_NAME,null,null); */
        }

        @Override
        public void failure(RetrofitError error) {
            //Assume we have no connection, since error is null
            Log.d("asd",error.getMessage());
            mProgressDia.dismiss();
            if (error == null) {
                Toast.makeText(Student_Homepage.this, "No internet connection", Toast.LENGTH_SHORT).show();
                //Snackbar.make(findViewById(R.id.rootView), "No internet connection", Snackbar.LENGTH_SHORT).show();
            }
        }

    }
    //endregion

    //region Audio Take
    private void setRecordFab(){
        fabAudio.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    // startRecording();
                    Vibrator v = (Vibrator) Student_Homepage.this.getSystemService(Context.VIBRATOR_SERVICE);
                    v.vibrate(100);
                    Handler h = new Handler();
                    h.postDelayed(new Runnable(){@Override public void run(){}}, 100);
                    fabAudio.setTitle("Grabando...");
                    audioRecording.starRecording();
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP
                        || motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
                    fabMenu.collapse();
                    Toast.makeText(Student_Homepage.this, "Grabado finalizado", Toast.LENGTH_SHORT).show();
                    fabAudio.setTitle("Grabar Audio");
                    audioRecording.stopRecording();
                }
                return true;
            }
        });
    }

    private String mAudioURL;

    private File mAudioResponse;
    private Gpic_Entry mAudioEntry;
    @Override
    public void audioRecorded(final File audioResponse) {
        mAudioResponse = audioResponse;
        Log.d(LOG_TAG,"audio recorded");
        LatLng latLng = mapFragment.getCurrentLocation();
        mAudioEntry = new Gpic_Entry();
        mAudioEntry.setEntryLatitude(Double.toString(latLng.latitude));
        mAudioEntry.setEntryLongitude(Double.toString(latLng.longitude));
        mAudioEntry.setEntryType("2");
        Log.d(LOG_TAG,"start uploading");
        mProgressDia = ProgressDialog.show(Student_Homepage.this,"","Uploading audio...",true);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    //RETORNA LA URL DE LA CANCION QUE ES SUBIDA AL SERVIDOR, SI SUCEDE ALGUN ERROR DEBE BOTAR VACIO = ""
                    mAudioURL = audioUpload.uploadFile(audioResponse.getAbsolutePath(), mProgressDia);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            continueUpload();
                        }
                    });
                } catch (OutOfMemoryError e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(Student_Homepage.this, "Insufficient Memory!", Toast.LENGTH_SHORT).show();
                        }
                    });
                    mProgressDia.dismiss();
                }

            }


        }).start();
    }
    private void continueUpload() {
        mAudioEntry.setEntryValue(mAudioURL);
        if(!"".equals(mAudioURL)) {
            mDbHelper.addItem(mAudioEntry,mStudentID,workStateGPicAct.getName());
            mapFragment.addGpicEntMarker(mAudioEntry);
            boolean del = mAudioResponse.delete();
        }
    }

    //endregion

    //region Text Take
    private void stWriteText() {
        Student_TextEntry_Dialog mDialog = Student_TextEntry_Dialog.newInstance();
        mDialog.setListener(this);
        mDialog.show(getFragmentManager(),"Text Entry Dialog");
    }

    @Override
    public void TextEntryDialogClosed(String text) {
        if(!"".equals(text)){
            LatLng latLng = mapFragment.getCurrentLocation();
            Gpic_Entry gPicEnt = new Gpic_Entry();
            gPicEnt.setEntryLatitude(Double.toString(latLng.latitude));
            gPicEnt.setEntryLongitude(Double.toString(latLng.longitude));
            gPicEnt.setEntryType("3");
            gPicEnt.setEntryValue(text);

            mDbHelper.addItem(gPicEnt,mStudentID,workStateGPicAct.getName());
            mapFragment.addGpicEntMarker(gPicEnt);
        }else {
            Toast.makeText(Student_Homepage.this, "Texto vacío", Toast.LENGTH_SHORT).show();
        }
    }

    //endregion

    //region Tag Take
    private boolean test = true;

    private void stWriteTag() {
        Firebase testFB = new Firebase(MainActivity.fbRoot+"Active_Activities");
        if(test){
            Log.d(LOG_TAG,"Created gpicact");
            Gpic_Activity gPicAct = new Gpic_Activity();
            gPicAct.setDescription("This is it");
            gPicAct.setLatitude("11.02125603338779");
            gPicAct.setLongitude("-74.87014342099428");
            gPicAct.setName("Test u");
            gPicAct.setRadius("1300");
            gPicAct.setTeacherID("GpicTeacherAdmin");

            testFB.child(gPicAct.getName()).setValue(gPicAct);
            test = !test;
        }
        else{
            Log.d(LOG_TAG,"Deleted gpicact");
            testFB.child("Test u").setValue(null);
        }
    }
    //endregion

    //endregion

    private void checkUploadSubmit() {
        int networkState = StaticMethods.getConnectivityStatusString(this);
        int networkSetting = Integer.parseInt(LoginSharedPreference.getNetworkSetting(this));
        if(networkState != 0){
            if(networkSetting == 1){
                uploadSubmit();
            }else{
                if(networkState == 1){
                    uploadSubmit();
                }else{
                    Toast.makeText(this,"No Wifi Connection",Toast.LENGTH_SHORT).show();
                }
            }
        }else{
            Toast.makeText(this,"No internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadSubmit(){
        Firebase uploadFirebase = new Firebase(MainActivity.fbRoot+"Submits/"
                +workStateGPicAct.getName()+"/"+mStudentID+"/");

        ArrayList<String[]> data  = mDbHelper.readData();
        if(data.size() == 0){

        }else {
            String route = "";
            mProgressDia = new ProgressDialog(this);
            mProgressDia.setMessage("Uploading...\nPlease wait");
            mProgressDia.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDia.setMax(data.size());
            mProgressDia.setCancelable(false);
            mProgressDia.show();

            for (String[] row : data) {
                String tempRoute = "Submits/" + row[0] + "/" + row[1] + "/Entries/";
                if (!route.equals(tempRoute)) {
                    route = tempRoute;
                    uploadFirebase = new Firebase(MainActivity.fbRoot + route);
                    uploadFirebase.setValue(null);
                    uploadFirebase.getParent().child("studentID").setValue(row[1]);
                }
                Gpic_Entry gPicEnt = new Gpic_Entry();
                gPicEnt.setEntryKey(row[2]);
                gPicEnt.setEntryLatitude(row[3]);
                gPicEnt.setEntryLongitude(row[4]);
                gPicEnt.setEntryType(row[5]);
                gPicEnt.setEntryValue(row[6]);
                uploadFirebase.child(gPicEnt.getEntryKey()).setValue(gPicEnt);
                mDbHelper.removeGpicEntry(gPicEnt);
                mProgressDia.incrementProgressBy(1);
            }
            mProgressDia.dismiss();
        }
    }

    @Override
    public void onAddStep(String time, String location){
        Firebase pathUpload = new Firebase(MainActivity.fbRoot+"Submits/"+
                workStateGPicAct.getName()+"/"+mStudentID+"/Pathing/");
        pathUpload.child(time).setValue(location);
    }

    private void clearPathing() {
        Firebase pathUpload = new Firebase(MainActivity.fbRoot+"Submits/"+
                workStateGPicAct.getName()+"/"+mStudentID+"/Pathing/");
        pathUpload.setValue(null);
    }

    @Override
    public void onDeleteEntry(Gpic_Entry gPicEnt) {
        mDbHelper.removeGpicEntry(gPicEnt);
        mapFragment.removeGPicEntMarker(gPicEnt);
    }
}
