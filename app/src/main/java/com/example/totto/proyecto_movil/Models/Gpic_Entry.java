package com.example.totto.proyecto_movil.Models;

/**
 * Created by Totto on 29/04/2016.
 */

import java.io.Serializable;

@SuppressWarnings("serial")
public class Gpic_Entry implements Serializable {

    private String entryLatitude;
    private String entryLongitude;
    private String entryType;
    private String entryValue;
    private String entryKey;

    public Gpic_Entry() {

    }

    public Gpic_Entry(String entryLatitude, String entryLongitude, String entryType, String entryValue, String entryKey) {
        this.entryLatitude = entryLatitude;
        this.entryLongitude = entryLongitude;
        this.entryType = entryType;
        this.entryValue = entryValue;
        this.entryKey = entryKey;
    }

    public String getEntryKey() {
        return entryKey;
    }

    public void setEntryKey(String entryKey) {
        this.entryKey = entryKey;
    }

    public String getEntryLatitude() {
        return entryLatitude;
    }

    public void setEntryLatitude(String entryLatitude) {
        this.entryLatitude = entryLatitude;
    }

    public String getEntryLongitude() {
        return entryLongitude;
    }

    public void setEntryLongitude(String entryLongitude) {
        this.entryLongitude = entryLongitude;
    }

    public String getEntryType() {
        return entryType;
    }

    public void setEntryType(String entryType) {
        this.entryType = entryType;
    }

    public String getEntryValue() {
        return entryValue;
    }

    public void setEntryValue(String entryValue) {
        this.entryValue = entryValue;
    }
}