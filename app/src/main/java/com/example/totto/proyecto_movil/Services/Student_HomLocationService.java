package com.example.totto.proyecto_movil.Services;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.totto.proyecto_movil.Models.Gpic_Activity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Student_HomLocationService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private Location mLastLocation;
    private List<Gpic_Activity> mData;
    private boolean executeOnce;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private mLocationListener locationListener;

    private final String LOG_TAG = "asdHomLocationService";

    public Student_HomLocationService() {
    }

    private class mLocationListener implements LocationListener {

        public mLocationListener(){        }

        @Override
        public void onLocationChanged(Location location) {
            mLastLocation.set(location);
            checkActivityDistance();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //### Service called to start/stop the Location Fragment change###//
        if(intent.hasExtra("startLocationService")){
            if(intent.getBooleanExtra("startLocationService",false)){
                if(!mGoogleApiClient.isConnected()) mGoogleApiClient.connect();
            }else{
                stopLocationService();
            }
        }
        //### Service is called from add/remove child listener ###//
        if(intent.hasExtra("gPicAct")) {
            int action = intent.getIntExtra("action",0);
            Gpic_Activity gPicAct = (Gpic_Activity) intent.getExtras().get("gPicAct");
            switch (action){
                case 1:
                    mData.add(gPicAct);
                    break;
                case -1:
                    mData.remove(getIndexOf(gPicAct));
            }
            if(!"".equals(mLastLocation.getProvider())) checkActivityDistance();
        }

        Log.e(LOG_TAG,"Service Started");
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLastLocation = new Location("");
        locationListener = new mLocationListener();
        mData = new ArrayList<>();
        Log.e(LOG_TAG,"Service Created");
    }

    //region GoogleApiClient Methods
    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLocationService();
        Log.e(LOG_TAG,"Service Destroyed");
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(40 * 1000);
        mLocationRequest.setFastestInterval(20 * 1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ActivityCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, locationListener);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(LOG_TAG,"GoogleApiClient Connection Suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(LOG_TAG,"GoogleApiClient Connection Failed");
    }
    //endregion

    //### Methods ###//

    private void stopLocationService(){
        if(mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, locationListener);
            mGoogleApiClient.disconnect();
        }
    }

    public int getIndexOf(Gpic_Activity gPicAct){
        int i = 0;
        for (Gpic_Activity activity : mData){
            if(activity.getName().equals(gPicAct.getName())){
                return i;
            }
            else{
                i++;
            }
        }
        return -1;
    }

    public void checkActivityDistance(){
        new CheckAllActivitiesTask().execute(mData);
    }

    private boolean lastWorkState = false;
    private class CheckAllActivitiesTask extends AsyncTask<List<Gpic_Activity>, Boolean, Gpic_Activity>{

        @Override
        protected Gpic_Activity doInBackground(List<Gpic_Activity>... params) {
            List<Gpic_Activity> data = params[0];
            for (Gpic_Activity gPicAct : data){
                int radius = Integer.parseInt(gPicAct.getRadius());
                double lat = Double.parseDouble(gPicAct.getLatitude());
                double lng = Double.parseDouble(gPicAct.getLongitude());
                Location location = new Location("");
                location.setLatitude(lat);
                location.setLongitude(lng);
                if(mLastLocation.distanceTo(location) < radius){
                    return gPicAct;
                }
            }
            return null;
        }


        @Override
        protected void onPostExecute(Gpic_Activity gPicAct) {
            if(gPicAct != null){
                if(!lastWorkState){
                    Log.d(LOG_TAG,"changing to work fragment");
                    lastWorkState = true;
                    Intent intent = new Intent("GpsLocationUpdate");
                    intent.putExtra("locationState",gPicAct);
                    LocalBroadcastManager.getInstance(Student_HomLocationService.this).sendBroadcast(intent);
                }
            }
            else{
                if(lastWorkState){
                    Log.d(LOG_TAG,"changin to rec fragment");
                    lastWorkState = false;
                    Intent intent = new Intent("GpsLocationUpdate");
                    intent.putExtra("locationState",gPicAct);
                    LocalBroadcastManager.getInstance(Student_HomLocationService.this).sendBroadcast(intent);
                }
            }

        }
    }
}

